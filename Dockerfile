ARG PHP_VERSION=8.2

FROM php:${PHP_VERSION}-fpm-alpine

RUN apk update && apk upgrade

RUN apk add bash autoconf g++ rabbitmq-c-dev make linux-headers

RUN printf "\n" | pecl install amqp

RUN docker-php-ext-install sockets

RUN docker-php-ext-enable amqp

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /app

COPY ./composer.* ./

RUN composer install \
    --no-interaction \
    --prefer-dist \
    --no-scripts \
    --no-autoloader

COPY --chown=www-data:www-data ./ ./

RUN composer dump-autoload --optimize