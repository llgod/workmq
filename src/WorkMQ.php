<?php


namespace llgod;

use Carbon\Carbon;
use Closure;
use Exception;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Illuminate\Support\Str;
use ReflectionMethod;


/**
 * The WorkMQ class provides functionality for initializing and managing a worker that listens on a specified queue.
 * It allows for dynamic configuration of the worker environment and handles the message processing workflow.
 */
class WorkMQ
{
    /**
     * Sets a callback for dynamically setting attributes during runtime. This callback allows
     * for adjusting the behavior or properties of the class based on external or runtime conditions.
     * These attributes, once set, can influence how messages are processed and can be accessed later
     * using the getter callback to modify or retrieve state information during message processing.
     *
     * @param Closure $closure The callback that defines how attributes should be dynamically set.
     * @return void
     */
    public static function setAttributesSetterCallback(Closure $closure): void
    {
        self::$attributesSetterCallback = $closure;
    }

    /**
     * Sets a callback for dynamically retrieving attributes during runtime. This callback functions
     * to access or modify the state of the application, particularly during message processing.
     * It allows for the retrieval of attributes set by the setter callback, providing a way to manage
     * state information that may influence the processing logic or behavior dynamically.
     *
     * @param Closure $closure The callback that defines how to dynamically retrieve attributes.
     * @return void
     */
    public static function setAttributesGetterCallback(Closure $closure): void
    {
        self::$attributesGetterCallback = $closure;
    }


    /**
     * Initializes and starts a worker that listens on a specified queue.
     * The method configures the worker environment based on provided options and manages the message processing workflow.
     * 
     * @param string $queue The name of the queue to listen to.
     * @param array{
     *      prefetch?: int,
     *      assure?: bool,              
     *      onerror?: string,           
     *      delay?: int,                
     *      requeueMode?: string,       
     *      maxWorkCount?: int,        
     *      forceDisconnectAfterOneJob?: bool,
     *      rateLimitSeconds?: int,   
     *      rateLimitCount?: int,    
     * } $options Configuration options for the worker. Defaults to an empty array if no options are specified.
     * 
     * @return void
     */
    public static function work(String $queue, array $options = []): void
    {
        $workerID = Str::uuid()->toString();
        $prefetch = (int) ($options['prefetch'] ?? 0);
        $assure = (bool) ($options['assure'] ?? false);
        $onerror = (string) ($options['onerror'] ?? 'requeue');
        $delay = (int) ($options['delay'] ?? 0);
        $requeueMode = (string) ($options['requeueMode'] ?? 'requeue');
        $maxWorkCount = (int) ($options['maxWorkCount'] ?? 0);
        //isto não deveria ser utilizado em produção
        $forceDisconnectAfterOneJob = (bool) ($options['forceDisconnectAfterOneJob'] ?? false);

        $rateLimitSeconds = ($options['rateLimitSeconds'] ?? 0);
        $rateLimitCount = ($options['rateLimitCount'] ?? 0);

        if ($prefetch > 0) {
            self::setQos($prefetch);
        }

        if ($delay > 0) {
            $consumerCount = self::getConsumerCount($queue);
            $delay = self::getFibonacci($consumerCount + 2);
        }

        $currentWorkCount = 0;
        $rateLimitConsumerCurrentCount = 0;
        $nextTick = null;

        self::consume($queue, function (AMQPMessage $message) use ($workerID, $assure, $onerror, $delay, $requeueMode, $maxWorkCount, &$currentWorkCount, $queue, &$rateLimitConsumerCurrentCount, &$nextTick, $rateLimitCount, $rateLimitSeconds) {
            self::handleMaxWork($currentWorkCount, $maxWorkCount);
            self::handleRateLimit($rateLimitConsumerCurrentCount, $nextTick, $rateLimitCount, $rateLimitSeconds);
            $messageBody = unserialize($message->getBody());
            $replyTo = $messageBody['__replyTo'] ?? null;
            $messageID = $messageBody['__messageID'] ?? null;
            try {
                $response = self::processMessage($message, $delay, $workerID);
                //handles delay
                if ($delay > 0 && is_string($response) && $response === '__delay__') {
                    return self::handleDelayed($message, $queue, $requeueMode);
                }

                if ($replyTo) {
                    return self::handleReplyTo($message, $replyTo, $response, $messageID);
                }
                //handles assure
                if ($assure) {
                    return self::handleAssure($message, $response, $queue, $onerror, $requeueMode);
                }

                $message->ack();
            } catch (\Throwable $th) {
                if ($replyTo) {
                    return self::handleReplyTo($message, $replyTo, null, $messageID, $th);
                }
                if ($onerror === 'requeue') {
                    if ($requeueMode === 'nack') {
                        return $message->nack(true);
                    } else if ($requeueMode === 'requeue') {
                        return self::basicPublish($queue, $message);
                    }
                } else if ($onerror === 'ignore') {
                    return $message->ack();
                }
                throw $th;
            }
        }, false, null, $forceDisconnectAfterOneJob);
    }


    /**
     * Sends a response back to the message's origin if a reply-to field is set.
     * This is crucial for RPC-style messaging where the sender expects a response.
     *
     * @param AMQPMessage $message The original message.
     * @param string|null $replyTo The queue to send the response to.
     * @param mixed $response The response to send.
     * @param string|null $messageID Unique identifier of the original message, used for correlation.
     * @return void
     */
    private static function handleReplyTo(AMQPMessage $message, String $replyTo, $response, $messageID, ?\Throwable $exception = null): void
    {

        $errorData = null;
        if ($exception) {
            $errorData = [
                "message" => $exception->getMessage()
            ];
        }
        $message = self::parseMessage([
            '__correlationID' => $messageID,
            '__response' => $response,
            '__errorData' => $errorData
        ]);
        self::basicPublish($replyTo, $message, true);
    }

    /**
     * Handles the assurance of message processing.
     *
     * @param AMQPMessage $message The message to handle.
     * @param mixed $response The response of the message processing.
     * @param string $queue The name of the queue.
     * @param string $onerror The action to take when an error occurs.
     * @param string $requeueMode The requeue mode to use.
     * @throws \Exception When an invalid requeue mode is provided.
     */
    private static function handleAssure(AMQPMessage $message, $response, string $queue, string $onerror, string $requeueMode): void
    {
        if ($response) {
            $message->ack();
        } else {
            if ($onerror === 'requeue') {
                if ($requeueMode === 'requeue') {
                    self::basicPublish($queue, $message);
                } else if ($requeueMode === 'nack') {
                    $message->nack(true);
                } else {
                    throw new \Exception("Invalid Requeue Mode {$requeueMode}", 1);
                }
            }
        }
    }

    /**
     * Handles delayed messages based on the requeue mode.
     *
     * @param AMQPMessage $message The message to handle.
     * @param string $queue The name of the queue.
     * @param string $requeueMode The requeue mode ('requeue' or 'nack').
     * @throws \Exception If the requeue mode is invalid.
     * @return void
     */
    private static function handleDelayed(AMQPMessage $message, string $queue, string $requeueMode): void
    {
        if ($requeueMode === 'requeue') {
            self::basicPublish($queue, $message);
        } else if ($requeueMode === 'nack') {
            $message->nack(true);
        } else {
            throw new \Exception("Invalid Requeue Mode {$requeueMode}", 1);
        }
    }

    /**
     * Processes an incoming message, applying any necessary delays and generating a response based on the message content.
     * Handles the logic defined for message processing including delays if configured.
     *
     * @param AMQPMessage $message The message to process.
     * @param int $delay Time in milliseconds to delay the message.
     * @param string $workerID Unique identifier for the worker.
     * @return mixed The response from processing the message or a delay indicator.
     */
    public static function processMessage(AMQPMessage $message, int $delay)
    {
        $messageBody = unserialize($message->getBody());

        self::setAttributes($messageBody['__attributes']);
        
        if($publishedAt = $messageBody['__publishedAt'] ?? null){
            if (self::shouldDelay($publishedAt, $delay)) {
                return '__delay__';
            }
        }

        $class = $messageBody['__class'];
        $function = $messageBody['__function'];
        $args = $messageBody['__args'];

        $reflection = new ReflectionMethod($class, $function);
        if ($reflection->isStatic()) {
            return $reflection->invoke(null, ...$args);
        }
        return $reflection->invoke(new $class, ...$args);
    }


    /**
     * Determines if a message should be delayed based on the published time and delay duration.
     *
     * @param Carbon $publishedAt The time the message was published.
     * @param int $delay The delay duration in seconds.
     * @return bool Returns true if the message should be delayed, false otherwise.
     */
    public static function shouldDelay(Carbon $publishedAt, int $delay): bool
    {
        if ($delay === 0) {
            return false;
        }
        return $publishedAt->addSeconds($delay)->isAfter(Carbon::now());
    }

    /**
     * Handles maximum work count to prevent overload where might be memory leaks.
     * This function stops processing new messages if the worker has reached its maximum message handling capacity.
     *
     * @param int &$currentWorkCount Reference to the current work count.
     * @param int $maxWorkCount Maximum number of messages a worker can process.
     * @return void
     */
    public static function handleMaxWork(int &$currentWorkCount, int $maxWorkCount): void
    {
        if ($maxWorkCount > 0) {
            $currentWorkCount++;
            if ($currentWorkCount > $maxWorkCount) {
                exit(0);
            }
        }
    }

    /**
     * Calculates the next tick time based on the current time and rate limit.
     *
     * @param float $currentTime The current time in microseconds.
     * @param int $rateLimitSeconds The rate limit in seconds.
     * @return float The next tick time in microseconds.
     */
    private static function getNextTick(float $currentTime, int $rateLimitSeconds): float
    {
        return $currentTime + $rateLimitSeconds;
    }

    /**
     * Handles rate limit for job processing.
     * This function sleeps if the worker has reached its maximum rate limit.
     *
     * @param int &$rateLimitConsumerCurrentCount reference to the current consume count.
     * @param float|null $nextTick Microtime of the next tick.
     * @param int $rateLimitCount Maximum number of messages a worker can process in a given time.
     * @param int $rateLimitSeconds Time to consider the rate limiting count.
     * @return void
     */
    private static function handleRateLimit(int &$rateLimitConsumerCurrentCount, ?float &$nextTick,int $rateLimitCount, int $rateLimitSeconds): void
    {
        //should be configured otherwise skip
        if(!$rateLimitCount || !$rateLimitSeconds){
            return;
        }

        $currentTime = microtime(true);

        if(!$nextTick || $currentTime > $nextTick){
            $nextTick = self::getNextTick($currentTime, $rateLimitSeconds);
            $rateLimitConsumerCurrentCount = 0;
            return;
        }

        $rateLimitConsumerCurrentCount++;
        
        if($rateLimitConsumerCurrentCount > $rateLimitCount){
            $sleepMicroseconds = ($nextTick - $currentTime) * 1000000;
            usleep((int)round($sleepMicroseconds));
        }
    }

    /**
     * Consume messages from a queue and process them using a closure.
     *
     * @param string $queue The name of the queue to consume from.
     * @param Closure $closure The closure to process the consumed messages.
     * @param bool $autoDelete (optional) Whether to automatically delete the queue after consuming. Default is false.
     * @param string|null $correlationID (optional) The correlation ID to filter consumed messages. Default is null.
     * @param bool $forceDisconnectAfterOneJob (optional) Whether to force disconnect after processing one job. Default is false.
     * @return void
     */
    private static function consume(String $queue, Closure $closure, $autoDelete = false, ?string $correlationID = null, bool $forceDisconnectAfterOneJob = false)
    {
        self::boot();
        self::declareQueue($queue, $autoDelete);

        $currentCorrelationID = null;

        $jobsDone = 0;

        $consumerTag = self::$channel->basic_consume($queue, '', false, false, false, false, function (AMQPMessage $message) use ($closure, $queue, &$currentCorrelationID, &$jobsDone) {
            $closure($message);
            $messageBody = unserialize($message->getBody());
            $currentCorrelationID = $messageBody['__correlationID'] ?? null;
            $jobsDone++;
        });
        while (self::$channel->is_consuming()) {
            if (($correlationID && $correlationID === $currentCorrelationID) || ($forceDisconnectAfterOneJob === true && $jobsDone === 1)) {
                self::$channel->basic_cancel($consumerTag);
            } else {
                self::$channel->wait();
            }
        }
    }

    /**
     * Calculates the nth number in the Fibonacci sequence.
     *
     * @param int $n The position of the number in the Fibonacci sequence.
     * @return int The nth number in the Fibonacci sequence.
     */
    private static function getFibonacci(int $n): int
    {
        return round(pow((sqrt(5) + 1) / 2, $n) / sqrt(5));
    }


    /**
     * Returns the number of consumers for a given queue.
     *
     * @param string $queue The name of the queue.
     * @return int The number of consumers for the queue.
     */
    public static function getConsumerCount(string $queue): int
    {
        self::boot();
        return self::declareQueue($queue)[2] ?? 0;
    }

    /**
     * Sets Quality of Service for the AMQP channel to limit the number of messages that can be processed.
     * This is particularly useful for controlling the flow of message processing based on the worker's capacity.
     *
     * @param int $prefetch The number of messages to prefetch.
     * @return void
     */
    private static function setQos(Int $prefetchCount = 10, Int $prefetchSize = 0, bool $aGlobal = false): void
    {
        self::boot();
        self::$channel->basic_qos($prefetchSize, $prefetchCount, $aGlobal);
    }

    /**
     * Asynchronously Remote Procedure Call (RPC) method.
     *
     * This method sends a message to a specified queue, containing information about the class, function, and arguments to be executed by a worker.
     *
     * @param string $queue The name of the queue to send the message to.
     * @param string $class The name of the class that contains the function to be called.
     * @param string $function The name of the function to be called.
     * @param mixed ...$args The arguments to be passed to the function.
     * @return void
     */
    public static function asyncRPC(String $queue, String $class, String $function, ...$args): void
    {
        $message = self::parseMessage([
            '__class' => $class,
            '__function' => $function,
            '__args' => $args,
            '__publishedAt' => Carbon::now(),
            '__messageID' => Str::uuid()->toString()
        ]);
        self::basicPublish($queue, $message);
    }

    /**
     * Synchronous Remote Procedure Call (RPC) method.
     *
     * This method sends a message to a specified queue, containing information about the class, function, and arguments to be executed by a worker.
     * It then waits for the worker to respond with the result or an error.
     *
     * @param string $queue The name of the queue to send the message to.
     * @param string $class The name of the class to be executed by the worker.
     * @param string $function The name of the function to be executed by the worker.
     * @param mixed ...$args The arguments to be passed to the function.
     * @return mixed The response from the worker.
     * @throws \Exception If an error occurs during the RPC call.
     */
    public static function syncRPC(String $queue, String $class, String $function, ...$args)
    {
        // Generate a unique message ID and replyTo queue name
        $messageID = Str::uuid()->toString();
        $replyTo = '_temp_' . Str::uuid()->toString();

        // Create the message payload with class, function, arguments, and metadata
        $message = self::parseMessage([
            '__class' => $class,
            '__function' => $function,
            '__args' => $args,
            '__publishedAt' => Carbon::now(),
            '__messageID' => $messageID,
            '__replyTo' => $replyTo
        ]);

        // Publish the message to the specified queue
        self::basicPublish($queue, $message);

        // Initialize response and error data variables
        $response = null;
        $errorData = null;

        // Consume the worker response from the replyTo queue
        self::consume($replyTo, function (AMQPMessage $workerResponse) use (&$response, &$errorData) {
            $responseBody = unserialize($workerResponse->getBody());
            $response = $responseBody['__response'];
            $errorData = $responseBody['__errorData'];
        }, true, $messageID);

        // Delete the replyTo queue
        self::$channel->queue_delete($replyTo);

        // If an error occurred, throw an exception
        if ($errorData) {
            $exception =  new \Exception($errorData["message"]);
            throw $exception;
        }

        // Return the response from the worker
        return $response;
    }

    /**
     * Parses a message and returns an instance of AMQPMessage.
     *
     * @param array $message The message to be parsed.
     * @param bool $setAttributes Whether to set attributes for the message.
     * @return AMQPMessage The parsed message as an instance of AMQPMessage.
     */
    private static function parseMessage(array $message, bool $setAttributes = true): AMQPMessage
    {

        if ($setAttributes) {
            $message['__attributes'] = self::getAttributes();
        }

        return new AMQPMessage(serialize($message), [
            'content-type' => 'application/php-serialized',
            'delivery_mode' => 2
        ]);
    }

    private static function getAttributes(): array
    {
        if (self::$attributesSetterCallback !== null) {
            $callback = self::$attributesSetterCallback;
            return $callback();
        }
        return [];
    }

    private static function setAttributes(array $attributes): void
    {
        if (self::$attributesGetterCallback !== null) {
            $callback = self::$attributesGetterCallback;
            $callback($attributes);
        }
    }

    /**
     * Publishes a message to a queue.
     *
     * @param string $queue The name of the queue to publish the message to.
     * @param AMQPMessage $message The message to be published.
     * @param bool $autoDelete (optional) Whether the queue should be automatically deleted after publishing the message. Default is false.
     * @return bool Returns true if the message was successfully published, false otherwise.
     */
    public static function basicPublish(String $queue, AMQPMessage $message, $autoDelete = false): bool
    {
        self::boot();
        self::declareQueue($queue, $autoDelete);
        self::$channel->basic_publish($message, '', $queue);
        return true;
    }

    /**
     * Declares a queue.
     *
     * @param string $queue The name of the queue to declare.
     * @param bool $autoDelete (optional) Whether the queue should be automatically deleted when it is no longer in use. Default is false.
     * @return array An array containing information about the declared queue.
     */
    private static function declareQueue(String $queue, bool $autoDelete = false): array
    {
        return self::$channel->queue_declare($queue, false, true, false, $autoDelete);
    }

    /**
     * Boots the WorkMQ class.
     *
     * This method is responsible for initializing the WorkMQ class by connecting to the server
     * and opening a channel. It ensures that the booting process is only executed once.
     *
     * @return void
     */
    private static function boot(): void
    {
        if (self::$isBooted) {
            return;
        }
        self::connect();
        self::openChannel();
        self::$isBooted = true;
    }

    /**
     * Establishes a connection to the AMQP server.
     *
     * @return void
     */
    private static function connect(): void
    {
        self::$connection = new AMQPStreamConnection(
            env('AMQP_HOST'),
            env('AMQP_PORT'),
            env('AMQP_USERNAME'),
            env('AMQP_PASSWORD'),
            '/',
            false,
            'AMQPLAIN',
            null,
            'en_US',
            10,
            10,
            null,
            true,
            0,
            0,
            null
        );
    }

    /**
     * Opens a channel for communication with the message queue.
     *
     * @return void
     */
    private static function openChannel(): void
    {
        self::$channel = self::$connection->channel();
    }



    private static AMQPStreamConnection $connection; // Connection instance to the AMQP server
    private static AMQPChannel $channel; // Channel instance for message communication
    private static bool $isBooted = false; // Indicates if the system has been initialized
    private static ?Closure $attributesSetterCallback = null; // Callback for setting dynamic attributes
    private static ?Closure $attributesGetterCallback = null; // Callback for getting dynamic attributes
}
