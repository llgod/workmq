<?php

namespace llgod\examples;

class JobsExample
{
    /**
     * Joins multiple arguments into a single string.
     *
     * @param mixed ...$args The arguments to join.
     * @return string The joined string.
     */
    public static function joinArgs(...$args): string
    {
        $string = '';
        foreach ($args as $arg) {
            $string = $string . $arg;
        }
        return $string;
    }

    /**
     * Creates a new file with the given identifier.
     *
     * @param string $identifier The identifier for the file.
     * @return void
     */
    public static function touchFile($identifier): void
    {
        touch("/tmp/$identifier");
    }

    /**
     * Checks if a file with the given identifier exists.
     *
     * @param string $identifier The identifier for the file.
     * @return bool True if the file exists, false otherwise.
     */
    public static function existsFile($identifier): bool
    {
        return file_exists("/tmp/$identifier");
    }

    /**
     * Requeues a job if a specific file exists.
     *
     * @param string $identifier The identifier for the job.
     * @throws \Exception If the file does not exist.
     * @return void
     */
    public static function requeueOnce($identifier): void
    {
        if (!file_exists("/tmp/a_$identifier")) {
            touch("/tmp/a_$identifier");
            throw new \Exception("File does not exists__", 1);
        }
        touch("/tmp/$identifier");
    }

    /**
     * Throws a custom error.
     *
     * @throws \PhpAmqpLib\Exception\AMQPIOException
     * @return void
     */
    public static function customError(): void
    {
        throw new \PhpAmqpLib\Exception\AMQPIOException("test_only__");
    }
}
