# llgod/WorkMQ

![https://gitlab.com/llgod/workmq/-/pipelines](https://gitlab.com/llgod/workmq/badges/main/pipeline.svg) ![https://gitlab.com/llgod/workmq/-/releases](https://gitlab.com/llgod/workmq/-/badges/release.svg?order_by=release_at)


`llgod/WorkMQ` is a Lumen package designed to facilitate the integration and abstraction of AMQP for easier message queuing operations within Lumen projects. It provides a set of utilities to handle asynchronous and synchronous remote procedure calls (RPC) through AMQP.

## Features

- **Asynchronous RPC**: Allows asynchronous aynchronous remote function execution via message queues.
- **Synchronous RPC**: Allows synchronous remote function execution via message queues.
- **Custom Attribute Handling**: Set custom attributes through callbacks.

## License

This project is licensed under the AGPL-3.0 License - see the [LICENSE](LICENSE) file for details.

## Installation

Install the package via Composer:

```bash
composer require llgod/WorkMQ
```

## Usage

### Asynchronous RPC

To perform an asynchronous RPC, use the `asyncRPC` method. This method allows you to send a message to a specific queue and handle the response asynchronously.

```php
use llgod\WorkMQ;

WorkMQ::asyncRPC(
    'your_queue_name',
    'TargetClass',
    'targetMethod',
    $arg1, $arg2, ...
);
```

### Synchronous RPC

For synchronous RPC, where the response is immediately required after the message is sent, use the `syncRPC` method.

```php
use llgod\WorkMQ;

$response = WorkMQ::syncRPC(
    'your_queue_name',
    'TargetClass',
    'targetMethod',
    $arg1, $arg2, ...
);
```

### Work Queue Consumer

To start a worker that listens to a queue, use the `work` method. This method allows for various configurations to manage how messages are consumed.

```php
use llgod\WorkMQ;

WorkMQ::work('your_queue_name', [
    'prefetch' => 1,
    'assure' => true,
    'onerror' => 'requeue',
    'delay' => 0,
    'requeueMode' => 'nack',
    'maxWorkCount' => 100,
    'forceDisconnectAfterOneJob' => false,
    'rateLimitSeconds' => 5,
    'rateLimitCount' => 10
]);
```

### Attribute Callbacks

Set or get custom attributes using the callback setters.

```php
use llgod\WorkMQ;

WorkMQ::setAttributesSetterCallback(function ($attributes) {
    // modify attributes here
});

WorkMQ::setAttributesGetterCallback(function () {
    // return attributes here
});
```


## Testing
To test this package you need to run the docker-compose.yaml services, in the php container execute the command ```/app/vendor/bin/phpunit```

## Contributing

Contributions are welcome! Please contact me on [allangodoy25@icloud.com](mailto:allangodoy25@icloud.com)
```