<?php

use llgod\WorkMQ;
use PhpAmqpLib\Message\AMQPMessage;
use PHPUnit\Framework\TestCase;

/**
 * Class WorkMQTest
 * 
 * This class contains unit tests for the WorkMQ class.
 */
class WorkMQTest extends TestCase
{
    /**
     * Test the attributes setter callback.
     */
    public function testAttributesSetter(): void
    {
        WorkMQ::setAttributesSetterCallback(function () {
            return ['__tested__'];
        });

        $reflection = new ReflectionClass(WorkMQ::class);
        $prop = $reflection->getProperty('attributesSetterCallback');
        $prop->setAccessible(true);
        $callback = $prop->getValue();

        $this->assertEquals(['__tested__'], $callback());
    }

    /**
     * Test the attributes getter callback.
     */
    public function testAttributesGetter(): void
    {
        WorkMQ::setAttributesGetterCallback(function ($var) {
            return $var;
        });

        $reflection = new ReflectionClass(WorkMQ::class);
        $prop = $reflection->getProperty('attributesGetterCallback');
        $prop->setAccessible(true);
        $callback = $prop->getValue();

        $this->assertEquals(['__tested__'], $callback(['__tested__']));
    }

    /**
     * Test the parseMessage method without attributes.
     */
    public function testParseMessageWihtoutAttributes(): void
    {

        $reflection = new ReflectionClass(WorkMQ::class);
        $method = $reflection->getMethod('parseMessage');
        $method->setAccessible(true);
        $body = ['__tested__' => '__tested__'];
        $message = $method->invoke(null, $body, false);
        $manualMessage = new AMQPMessage(serialize($body), [
            'content-type' => 'application/php-serialized',
            'delivery_mode' => 2
        ]);

        $this->assertEquals($message, $manualMessage);
    }

    /**
     * Test the parseMessage method with attributes.
     */
    public function testParseMessageWihtAttributes():  void
    {

        WorkMQ::setAttributesSetterCallback(function () {
            return ['__tested__'];
        });
        $reflection = new ReflectionClass(WorkMQ::class);
        $method = $reflection->getMethod('parseMessage');
        $method->setAccessible(true);
        $body = ['__tested__' => '__tested__'];
        $message = $method->invoke(null, $body, true);
        $body['__attributes'] = ['__tested__'];
        $manualMessage = new AMQPMessage(serialize($body), [
            'content-type' => 'application/php-serialized',
            'delivery_mode' => 2
        ]);

        $this->assertEquals($message, $manualMessage);
    }

    /**
     * Test the worker count.
     */
    public function testWorkerCount():  void
    {
        $this->assertEquals(1, WorkMQ::getConsumerCount('_example_queue'), "You should start a worker before running the tests");
    }

    /**
     * Test asynchronous RPC.
     */
    public function testAsyncRpc():  void
    {
        $identifier = uniqid();
        WorkMQ::asyncRPC("_example_queue", llgod\examples\JobsExample::class, 'touchFile', $identifier);
        sleep(1);
        $this->assertFileExists("/tmp/$identifier");
    }

    /**
     * Test synchronous RPC.
     */
    public function testSyncRpc():  void
    {
        $response = WorkMQ::syncRPC('_example_queue', llgod\examples\JobsExample::class, 'joinArgs', 'Hello', ' World');
        $this->assertEquals($response, "Hello World");
    }

    /**
     * Test requeue asynchronous.
     */
    public function testRequeueAsync():  void
    {
        $identifier = uniqid();
        WorkMQ::asyncRPC("_example_queue", llgod\examples\JobsExample::class, 'requeueOnce', $identifier);
        sleep(1);
        $this->assertFileExists("/tmp/$identifier");
    }

    /**
     * Test error in synchronous RPC.
     */
    public function testErrorSync():  void
    {
        $this->expectExceptionMessage("File does not exists__");
        $identifier = uniqid();
        WorkMQ::syncRPC("_example_queue", llgod\examples\JobsExample::class, 'requeueOnce', $identifier);
    }

    /**
     * Test custom error in synchronous RPC.
     */
    public function testErrorSyncCustom():  void
    {
        $this->expectExceptionMessage("test_only__");
        WorkMQ::syncRPC("_example_queue", llgod\examples\JobsExample::class, 'customError');
    }
}
